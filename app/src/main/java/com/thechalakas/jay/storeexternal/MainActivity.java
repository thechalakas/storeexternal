package com.thechalakas.jay.storeexternal;

/*
 * Created by jay on 20/09/17. 8:49 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonstore = (Button) findViewById(R.id.button);
        buttonstore.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonstore - click reached");

                if(isExternalStorageWritable())
                {
                    Log.i("MainActivity","buttonstore - yes you can write and read");
                    BufferedReader input = null;
                    File file = null;
                    try {
                        file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"hellotext2");

                        //file = new File(getCacheDir(), "MyCache"); // Pass getFilesDir() and "MyFile" to read file

                        input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                        String line;
                        StringBuffer buffer = new StringBuffer();
                        while ((line = input.readLine()) != null)
                        {
                            buffer.append(line);
                        }
                    Log.i("MainActivity","file was read successfully from " + file.toString());
                        Log.i("MainActivity","file contents are -  " + buffer);

                }
                    catch (Exception e)
                {
                    Log.i("MainActivity","11Some issue with FileOutputStream --- " + e.toString());
                }
                }
                else

                {
                    Log.i("MainActivity","buttonstore - YOU CANNOT write and read");
                }
            }
        });

        Button buttonLoad = (Button) findViewById(R.id.button2);
        buttonLoad.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonLoad - click reached");

                if(isExternalStorageWritable())
                {
                    Log.i("MainActivity","buttonLoad - yes you can write and read");

                    String content = "hello world";
                    File file;
                    FileOutputStream outputStream;
                    try {
                        //file = new File(Environment.getExternalStorageDirectory(), "MyCache");
                        file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"hellotext2");

                        outputStream = new FileOutputStream(file);
                        outputStream.write(content.getBytes());
                        outputStream.close();
                        Log.i("MainActivity","file was written successfully to ");

                    }
                    catch (Exception e)
                    {
                        Log.i("MainActivity","11Some issue with FileOutputStream --- " + e.toString());
                    }
                }
                else

                {
                    Log.i("MainActivity","buttonLoad - YOU CANNOT write and read");
                }
            }
        });
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable()
    {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            return true;
        }
        return false;
    }

    public File getAlbumStorageDir(String albumName)
    {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), albumName);
        if (!file.mkdirs())
        {
            Log.e("MainActivity", "getAlbumStorageDir error Directory not created" + file.toString());
        }
        return file;
    }
}
